#!/usr/bin/python3

import os
from os import path
import sys
import subprocess
import argparse

parser = argparse.ArgumentParser()

subparsers = parser.add_subparsers(help="commands", dest="command")
build_parser = subparsers.add_parser("build")
push_parser = subparsers.add_parser("push")
run_parser = subparsers.add_parser("run")
run_parser.add_argument("run_args", nargs="*", help="run args")

args = parser.parse_args()
if args.command == None:
    parser.print_help()
    parser.exit(1)

script_dir: str = path.dirname(path.realpath(sys.argv[0]))

os.chdir(script_dir)

tag = "log-viewer-docker:latest"
registry = "registry.gitlab.com/berublan/vscode-log-viewer"

if args.command == 'build':
    subprocess.run(["docker", "build", "-t", tag, "."])

if args.command == 'push':
    subprocess.run(["docker", "tag", tag, registry])
    subprocess.run(["docker", "push", registry])

if args.command == 'run':
    repo_dir = path.realpath(path.join(script_dir, ".."))
    subprocess.run(["docker", "run", "--rm", "--interactive", "--tty",
                    "--volume", f"{repo_dir}:/mnt/src", tag, *args.run_args])


# interesting images:
# https://hub.docker.com/r/canvadev/ci-docker-node-yarn-chrome-xvfb
# https://hub.docker.com/r/chriscamicas/node-xvfb
